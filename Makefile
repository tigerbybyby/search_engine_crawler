TEST_VENV_NAME=testing_venv
TEST_PIP=${TEST_VENV_NAME}/bin/pip3

TEST_ISORT=${TEST_VENV_NAME}/bin/isort
TEST_FLAKE=${TEST_VENV_NAME}/bin/flake8
TEST_PYLINT=find . -type f -name "*.py" ! -path "./venv/*" ! -path "./testing_venv/*" | xargs ${TEST_VENV_NAME}/bin/pylint
TEST_MYPY=${TEST_VENV_NAME}/bin/mypy
TEST_BANDIT=${TEST_VENV_NAME}/bin/bandit
TEST_COVERAGE=${TEST_VENV_NAME}/bin/coverage

.DEFAULT: help

help:
	@echo "make test"
	@echo "       create testing virtualenv and run tests"
	@echo "make clean"
	@echo "       remove temporary files"

.testing-venv:
	@printf "Creating virtualenv...\t\t"
	@pip3 install -q virtualenv
	@virtualenv -q -p python3 $(TEST_VENV_NAME)
	@echo "\033[92m [OK] \033[0m"
	@printf "Installing requirements...\t"
	@${TEST_PIP} install -q -r requirements.txt -r requirements-test.txt
	@echo "[OK]"

.coverage:
	@printf "Running coverage checks...\t"
	@${TEST_COVERAGE} run test_crawler.py
	@printf "Generating coverage report...\t"
	@${TEST_COVERAGE} report -m



test: .testing-venv .coverage

clean:
	@rm -rf build dist .eggs *.egg-info .coverage
	@rm -rf ${TEST_VENV_NAME}
	@rm -rf coverage_html_report
	@find . -type d -name '.mypy_cache' -exec rm -rf {} +
	@find . -type d -name '*pytest_cache*' -exec rm -rf {} +

